Devitalize
==========

Devitalize (or make grayscaled) images of a .tex file to help adapting it to B&amp;W printing

## How to Use
This command makes all .eps images included in input.tex grayscaled and outputs input_mono.tex which properly reads the grayscaled images.
<pre><code>$ ./devitalize.sh input.tex
</code></pre>

Now you can use your usual build proceadure for a .tex file
<pre><code>$ platex input_mono.tex
$ dvipdf input_mono.dvi
... 
</code></pre>

## What is "devitalization" ?
The name "devitalize" is inspired from a song "Vitalization" (by Nana Mizuki, 2013).
It expresses un-vitality we feel when we see grayscaled pictures.
