# $1: input file

images=`grep -o "[a-z\|A-Z|_|0-9]\+\.eps" $1 | uniq`

for i in $images; do
    echo `basename $i .eps`_mono.eps
    convert -type GrayScale $i `basename $i .eps`_mono.eps
done

sed -e "s/\([a-zA-Z0-9_]\+\)\.eps/\1_mono.eps/g" $1 > `basename $1 .tex`_mono.tex
